﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using  UnityEngine.UI;
		
public class GenWordhead : MonoBehaviour
{	Button leftbutton;
	Button rightbutton ;
	
    // Start is called before the first frame update
    void Start()
    {
       if (Globals.currentscene==1)
		   wordhead(Globals.current_num);
		
    }

	
	
	void wordhead(int current_num)
    {

	GameObject add_btn;
	Button temp;
	string dir;
	string btn_name="wordhead";
	Sprite img,invisible ;
	for (int i=1;i<=6;i++){
		add_btn=GameObject.Find("wordhead"+i);
		//print(current_num);
		img=null;
		if (current_num<Path.radical.Length)
			img=Resources.Load <Sprite>("character/words/"+Path.radical[current_num].path+"/"+Path.radical[current_num].id);
		invisible=Resources.Load <Sprite>("resource/invisible");
		current_num++;
		
		string myScripts2 = "btnRespond"; 
		if (img!=null){
			add_btn.GetComponent<Image>().sprite = img;

			if(add_btn.GetComponent("btnRespond") == null){
				add_btn.AddComponent (System.Type.GetType (myScripts2));
				
			}
				
		}
		else {
			add_btn.GetComponent<Image>().sprite = invisible;
			Destroy(add_btn.GetComponent("btnRespond"));
		}
	}
	
	
   }
    

	
	public void leftbtn(){
		if (Globals.current_page>0){
			Globals.current_page--;
			Globals.current_num-=6;
		}
		if (Globals.current_num<=0)
			Globals.current_num=0;
		wordhead(Globals.current_num);
		
	
	}
	
	public void rightbtn(){



			
			if (Globals.current_page<Globals.last_page){
				Globals.current_page++;
				Globals.current_num+=6;
			}
			wordhead(Globals.current_num);
			print("68:"+Globals.current_num);
		
		
		
	}
    // Update is called once per frame
    void Update()
    {
       
    }
}
