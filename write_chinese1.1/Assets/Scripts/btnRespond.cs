﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class btnRespond : MonoBehaviour
{
	Button clickedBtn;
    // Start is called before the first frame update
    void Start()
    {
      clickedBtn = GetComponent<Button>();
      clickedBtn.onClick.AddListener(() => showID());
    }
	
	public void showID(){
		
		Globals.id=this.GetComponent<Image>().sprite.name;
		
		
		string myScripts = "GenWordhead"; 
		string myScripts2 = "GenWordpage";
		string myScripts3 = "Change_Text"; 		
		string myScripts4 = "draw"; 	
		
		GameObject wordhead=GameObject.Find("WordHead");
		GameObject level=GameObject.Find("Level");
		GameObject btngp=GameObject.Find("WordBtnGp");
		//GameObject character=GameObject.Find("char_frame");
		GameObject canvas=GameObject.Find("Canvas");
		GameObject leftbtn=GameObject.Find("Left");
		GameObject rightbtn=GameObject.Find("Right");
		
		if (Globals.currentscene==1){
			foreach (var item in Path.radical){
				if (item.id==Globals.id)
					Path.path=item.path;
			}
			if(wordhead.GetComponent("GenWordpage") != null){
				Destroy(wordhead.GetComponent("GenWordpage"));
				Destroy(wordhead.GetComponent("Change_Text"));
			}
			if(wordhead.GetComponent("GenWordhead") != null){
			
				Destroy(wordhead.GetComponent("GenWordhead"));
				wordhead.AddComponent (System.Type.GetType (myScripts2));
				wordhead.AddComponent (System.Type.GetType (myScripts3));
				
			}
			leftbtn.transform.position= new Vector3(-2500.0f,0.0f,0.0f);
			rightbtn.transform.position= new Vector3(-2500.0f,0.0f,0.0f);
			GameObject.Find("Right_2").transform.position= new Vector3(1.9f,-2.0f,0.0f);
			GameObject.Find("Left_2").transform.position= new Vector3(-1.9f,-2.0f,0.0f);
	
			
		}
		else if (Globals.currentscene==2){
			
			if(wordhead.GetComponent("GenWordhead") != null){
				//print("OKOK");
				Destroy(wordhead.GetComponent("GenWordhead"));
				wordhead.AddComponent (System.Type.GetType (myScripts2));
				if (wordhead.GetComponent("Change_Text") != null)
					wordhead.AddComponent (System.Type.GetType (myScripts3));

			}

			if (wordhead.GetComponent("GenWordpage")!=null){
				Destroy(wordhead.GetComponent("GenWordpage"));
				Destroy(wordhead.GetComponent("Change_Text"));
				Globals.currentscene=3;
				print("delelted");
				print("fine");
				string prefab="prefabs/Letters/"+Globals.id;
				print(prefab);
				Globals.char_name=Globals.id+"(Clone)";
				//canvas.GetComponent<draw>();
				canvas.GetComponent<draw>().enabled = true;
				//canvas.AddComponent (System.Type.GetType (myScripts4));
				GameObject instance = Instantiate(Resources.Load(prefab) as GameObject);
				
				print("fine");
				
				//character_real.AddComponent (System.Type.GetType (myScripts4));
				
			}
			btngp.transform.position= new Vector3(-2500.0f,0.0f,0.0f);
			level.transform.position= new Vector3(0.0f,0.0f,0.0f);
			//character.transform.position= new Vector3(0.0f,0.5f,1.0f);
			leftbtn.transform.position= new Vector3(-2500.0f,0.0f,0.0f);
			rightbtn.transform.position= new Vector3(-2500.0f,0.0f,0.0f);
			GameObject.Find("Right_2").transform.position= new  Vector3(-2500.0f,0.0f,0.0f);
			GameObject.Find("Left_2").transform.position= new  Vector3(-2500.0f,0.0f,0.0f);
			wordhead.AddComponent (System.Type.GetType (myScripts3));
		}
		
	
			
		
	}
    // Update is called once per frame
    void Update()
    {

    }
}
