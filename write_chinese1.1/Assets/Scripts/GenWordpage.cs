﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
	
	
public class GenWordpage : MonoBehaviour
{	
	
	
    // Start is called before the first frame update
    void Start()
    {
      
		wordpage(Globals.wordpage_current_num);
		Globals.currentscene=2;
		//print("okokokok"+Globals.currentscene);
		
    }

	
	
	void wordpage(int current_num)
    {

	GameObject add_btn;
	Button temp;
	string dir;
	string btn_name="wordhead";
	Sprite img,invisible ;
	Path.nature=loadJson.load("Assets/Resources/character/data/"+Path.path+".json");
	for (int i=1;i<=6;i++){
		add_btn=GameObject.Find("wordhead"+i);
		//print(current_num);
		print("34343434"+Path.path);
		
		
		Globals.wordpage_last_page=(Path.nature.Length/6)+1;
		print("39:"+Globals.wordpage_last_page);
	
		//print("37 hahaha"+Path.nature[i-1].path+"/"+Path.nature[i-1].id);
		img=null;
		if (current_num<Path.nature.Length)
			img=Resources.Load <Sprite>("character/words/"+Path.nature[current_num].path+"/"+Path.nature[current_num].id);
	
		invisible=Resources.Load <Sprite>("resource/invisible");
		
		current_num++;
		string myScripts2 = "btnRespond"; 
		if (img!=null){
			
			add_btn.GetComponent<Image>().sprite = img;
			if(add_btn.GetComponent("btnRespond") == null){
				add_btn.AddComponent (System.Type.GetType (myScripts2));
				
			}
				
		}
		else {
			add_btn.GetComponent<Image>().sprite = invisible;
			Destroy(add_btn.GetComponent("btnRespond"));
		}
	}
	
	
   }
    

	
	public void leftbtn(){
		if (Globals.wordpage_current_page>0){
			Globals.wordpage_current_page--;
			Globals.wordpage_current_num-=6;
		}
		if (Globals.wordpage_current_num<=0)
			Globals.wordpage_current_num=0;
		wordpage(Globals.wordpage_current_num);
		print("60:"+Globals.wordpage_current_num);
	
	}
	// last_page not yet set
	public void rightbtn(){
		if (Globals.wordpage_current_page<Globals.wordpage_last_page){
			Globals.wordpage_current_page++;
			Globals.wordpage_current_num+=6;
		}
		wordpage(Globals.wordpage_current_num);
		print("68:"+Globals.wordpage_current_num);
		
	}
    // Update is called once per frame
    void Update()
    {
       
    }
}
